SELECT Count(distinct order_id) order_id,
       sum(gmv) valor
FROM order_mart_dwd_order_item_all_event_final_status_df
WHERE logistics_status in ('NOT STARTED', 'PICKUP DONE', 'READY')
and buyer_id IN (298396713,336345991,410005707,323722889,374296344,341157740,296113981,305636929,300767060,251335001,287796267,213596490)
and date(split(pay_datetime, ' ')[1]) >= date('2019-01-01') 
