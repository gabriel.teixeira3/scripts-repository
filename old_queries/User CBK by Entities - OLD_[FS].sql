create table if not exists fba_fraud_cbk_entities as (


 WITH cbk as (
select    
               t2.buyer_id 
              ,t2.payment_transaction_id
              ,max_by(date((lpad(t4.pmt_confirm_date,10,'0'))), t4.ingestion_timestamp) pmt_confirm_date
              ,max_by(date((lpad(t4.chargeback_date,10,'0'))), t4.ingestion_timestamp) chargeback_date
              ,max_by(cast(t4.amount_chargeback as double), t4.ingestion_timestamp) amount_chargeback
              

    FROM  shopee_br.order_mart_dwd_order_all_event_final_status_df as t2 --ON t2.buyer_id  =  cast (ab.buyer_id as int)
    LEFT JOIN shopeepay.shopee_payment_module_payment_br_db__payment_v2_tab__br_continuous_s0_live t3 on t3.transaction_id = t2.payment_transaction_id
    LEFT JOIN shopee_br.shopee_br_bi_team__chargeback_ebanx t4 on t4.pmt_hash =  t3.channel_ref
       WHERE date(from_unixtime(cast(t4.ingestion_timestamp as int)- 39600)) <= current_date 
       GROUP BY 1,2
       HAVING max_by(cast(t4.amount_chargeback as double), t4.ingestion_timestamp) > 0
            and lower(max_by(t4.description, t4.ingestion_timestamp)) like '%fraud%'
     
)
 
 -- TONGDUN --
 
  SELECT    userid,
  lower("to_hex"("from_base64"(hash_shopee_df))) record,
            'tongdun device' as entity,
            sum(cast(amount_chargeback as real)) amount_chargeback

    FROM shopee_br.shopee_fraud_check_br_db__device_record_tab t1 
    INNER JOIN cbk as t2 on t2.buyer_id = t1.userid
        WHERE "to_hex"("from_base64"(hash_shopee_df)) <> ''
    GROUP BY 1,2,3
    UNION 
    
-- WEB DEVICE --

  SELECT    userid,
  lower(to_hex((hashed_fingerprint))) record,
            'web device' as entity,
            sum(cast(amount_chargeback as real)) amount_chargeback
    FROM shopee_br.shopee_device_extension_db__device_ext_tab t1 
    INNER JOIN cbk as t2 on t2.buyer_id = t1.userid
        WHERE to_hex((hashed_fingerprint)) <> ''
        AND extinfo.is_web = True 
        
    GROUP BY 1,2,3 
    UNION
    
-- ADDRESS --

  SELECT   
   t1.buyer_id
   , concat(lower(buyer_shipping_address),',',buyer_shipping_address_city) record,
            'address and city' as entity,
            sum(cast(amount_chargeback as real)) amount_chargeback
    FROM shopee_br.order_mart_dwd_order_all_event_final_status_df t1 
    INNER JOIN cbk as t2 on t2.buyer_id = t1.buyer_id
        WHERE concat(lower(buyer_shipping_address),',',buyer_shipping_address_city) <> ''
    GROUP BY 1,2,3
    
    UNION
    
-- CC FINGERPRINT --

  SELECT    t1.user_id,
  identifier record,
            'cc identifier' as entity,
            sum(cast(amount_chargeback as real)) amount_chargeback
    FROM shopee_br.shopee_payment_module_br_db__card_tab t1 
    INNER JOIN cbk as t2 on t2.buyer_id = t1.user_id
        WHERE identifier <> ''
    GROUP BY 1,2,3
    
    UNION 
    
-- PHONE --

  SELECT   t1.userid,
   phone record,
            'phone' as entity,
            sum(cast(amount_chargeback as real)) amount_chargeback
    FROM (SELECT userid,
    			 phone
    	FROM shopee_br_s1.shopee_account_v2_db__account_tab

    UNION
       
        SELECT userid,
    		   shipping_phone phone
    	FROM shopee_br_s1.shopee_order_v4_db__order_v4_tab) t1 
    INNER JOIN cbk as t2 on t2.buyer_id = t1.userid
        WHERE phone <> ''
    GROUP BY 1,2,3

    UNION

-- EMAIL --

  SELECT    
  t1.userid,
  email record,
            'email' as entity,
            sum(cast(amount_chargeback as real)) amount_chargeback
    FROM shopee_br_s1.shopee_account_v2_db__account_tab t1 
    INNER JOIN cbk as t2 on t2.buyer_id = t1.userid
        WHERE email <> ''
    GROUP BY 1,2,3

    UNION

-- CPF --
SELECT DISTINCT *

    FROM (
  SELECT DISTINCT
         t1.userid
       , cast(extinfo.tax_id as varchar) record
        ,'cpf' as entity
        ,sum(cast(amount_chargeback as real)) amount_chargeback
        FROM shopee_br_s1.shopee_account_v2_db__account_tab t1 
        INNER JOIN cbk as t2 on t2.buyer_id = t1.userid
        WHERE extinfo.tax_id  <> ''
    GROUP BY 1,2,3
    
        UNION
SELECT DISTINCT 
      t1.user_id
        , cast(json_extract(extra_data,'$.tax_id') as varchar) as record_
         ,'cpf' as entity
         ,sum(cast(amount_chargeback as real)) amount_chargeback
        FROM shopee_br.shopee_payment_module_transaction_br_db__transaction_v2_tab t1
        LEFT JOIN shopee_br.order_mart_dwd_order_all_event_final_status_df t2 on t2.payment_transaction_id = t1.transaction_id
        INNER JOIN cbk as t3 on t3.buyer_id = t1.user_id
         GROUP BY 1,2,3
    )
    WHERE record <> ''
)

