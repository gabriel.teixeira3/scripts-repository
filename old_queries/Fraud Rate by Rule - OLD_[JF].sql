with
fraud_check AS (
    select distinct 
        t1.user_id
        , from_unixtime(t3.ctime - 39600) action_datetime
        , t2.name fraud_type
    from shopee_br.user_mart_dim_user t1
        left join shopee_br.shopee_fraud_backend_br_db__fraud_user_tag_tab t3 on (t1.user_id = t3.userid)
        left join shopee_br.shopee_fraud_backend_br_db__fraud_tag_tab t2 ON (t2.id = t3.tagid)
    where t1.status <> 1
        and (t2.severity = 1)
        and (t2.status = 1)
        and date(t1.grass_date) = date(current_timestamp AT TIME ZONE 'America/Sao_Paulo' - INTERVAL  '24' HOUR) 
    )
, last_checkout as ( 
    with base as (
        select distinct 
            buyer_id 
            , checkout_id
            , case when order_be_status_id in (2,4,11,12,13,14,15) then 1 else 0 end as is_net
            , cast(create_datetime as TIMESTAMP) create_datetime
        from order_mart_dwd_order_all_event_final_status_df t1
            left join fraud_check t2 on t1.buyer_id = t2.user_id
        where cast(create_datetime as TIMESTAMP) < action_datetime
        )
    select distinct 
        buyer_id
        , max_by(checkout_id, create_datetime) last_checkout_id
        , is_net
    from base 
    group by 1, 3)
, fraud_base as (
    select distinct 
        date(from_unixtime(action_time_msec/1000) AT TIME ZONE'America/Sao_Paulo') action_date
        , target_pk as user_id
        , arbitrary(id) log_id
        ,cast(arbitrary(json_extract(changes, '$.status[0]')) AS VARCHAR) old_status
        ,cast(arbitrary(json_extract(changes, '$.status[1]')) AS VARCHAR) new_status
        ,arbitrary(json_extract_scalar(changes, '$.reason')) reason
        ,arbitrary(json_extract(changes, '$.reason')) remarks
        ,case when is_seller = 1 then 'yes'
            when is_seller = 0 then 'no'
        else null end seller_or_no
        , fraud_type
        , last_checkout_id
        , is_net
    from shopee_br.shopee_backend_log_br_db__shopee_audit_log_tab b
        left join shopee_br.user_mart_dim_user a on a.user_id = b.target_pk
        left join fraud_check c on b.target_pk = c.user_id and date(from_unixtime(b.action_time_msec/1000) AT TIME ZONE'America/Sao_Paulo') = date(c.action_datetime)
        left join last_checkout l on a.user_id = l.buyer_id
    where   target_tab = 'account_change_log'
        and   regexp_like(username, '@shopee')
        and   ( regexp_like(changes, '(?i)fraud') or json_extract(changes, '$.status[1]') is not null)
        --and date(from_unixtime(action_time_msec/1000) AT TIME ZONE'America/Sao_Paulo')  >= (current_date - interval '1' day )
    group by 1, 2, 8, 9, 10, 11
    ) 
select  distinct 
    date_trunc('week',action_date) week
    , log_id
    , user_id
    , fraud_type
    , last_checkout_id
    , is_net
    ,case when cast(remarks as VARCHAR) like '%BR_06%' then 'BR_06_rooted'
        when cast(remarks as VARCHAR) like '%BR_10%' then 'BR_10_ip_network'
        when cast(remarks as VARCHAR) like '%BR_13%' then 'BR_13_free_shipping_fraud'
        when cast(remarks as VARCHAR) like '%BR_2_%' then 'BR_2_many_orders'
        when cast(remarks as VARCHAR) like '%BR_25%' then 'BR_25_severe_chat_spam'
        when cast(remarks as VARCHAR) like '%BR_26%' then 'BR_26_bulk_registration'
        when cast(remarks as VARCHAR) like '%BR_28%' then 'BR_28_voucher_abuse_same_IP'
        when cast(remarks as VARCHAR) like '%BR_46%' then 'BR_46_return_refund_abuse'
        when cast(remarks as VARCHAR) like '%BR_54%' then 'BR_54_cc_check1a'
        when cast(remarks as VARCHAR) like '%BR_55%' then 'BR_55_cc_check2a'
        when cast(remarks as VARCHAR) like '%BR_57%' then 'BR_57_cc_check1b'
        when cast(remarks as VARCHAR) like '%BR_58%' then 'BR_58_cc_check2b'
        when cast(remarks as VARCHAR) like '%BR_59%' then 'BR_59_cc_check53'
        when cast(remarks as VARCHAR) like '%BR_60%' then 'BR_60_cc_check52'
        when cast(remarks as VARCHAR) like '%BR_62%' then 'BR_62_cc_check6'
        when cast(remarks as VARCHAR) like '%BR_63%' then 'BR_63_cc_check71'
        when cast(remarks as VARCHAR) like '%BR_64%' then 'BR_64_cc_check72'
        when cast(remarks as VARCHAR) like '%BR_65%' then 'BR_65_cc_check73'
        when cast(remarks as VARCHAR) like '%BR_66%' then 'BR_66_ship_check13'
        when cast(remarks as VARCHAR) like '%BR_67%' then 'BR_67_ship_check2'
        when cast(remarks as VARCHAR) like '%BR_68%' then 'BR_68_ship_check'
    else 'other checks' end as remarks
from    fraud_base
where new_status like 'banned' 
order by 1 desc
