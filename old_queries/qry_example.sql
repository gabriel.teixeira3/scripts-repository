select
    *
from 
    (
    select
        t1.request_id 
        , t1.user_id
        , t1.event_name
        , t1.event_timestamp
        , t1.event_local_datetime
        , t1.event_hit_resultn
        , t1.risk_assessment_type
        , t1.is_preconditions_hit
        , t1.is_rules_hit
        , t1.is_strategy_hit
        --, json_extract_scalar(t1.strategies, '$[1].strategy_id') as strategies
        , cast(json_extract_scalar(strategy, '$.strategy_id') as bigint) as strategy_id
        , cast(json_extract_scalar(strategy, '$.hit_status') as bigint) as strategy_hit
        , json_extract_scalar(strategy, '$.strategy_name') as strategy_name
        , cast(json_extract_scalar(strategy, '$.strategy_status') as bigint) as strategy_status        
        , cast(json_extract_scalar(rule, '$.rule_id') as bigint) as rule_id        
        , cast(json_extract_scalar(rule, '$.is_hit') as bigint) as rule_hit
        , json_extract_scalar(rule, '$.left_name') as rule_left_name
        , json_extract_scalar(rule, '$.right_name') as rule_right_name
        , json_extract_scalar(rule, '$.left_value') as rule_left_value
        , json_extract_scalar(rule, '$.right_value') as rule_right_value
        --, strategy
    from 
        antifraud_region.dwd_evt_rule_engine_all_strategies_exec_log_di__br t1
        , unnest(cast(json_parse(t1.strategies) as array<json>)) as t2(strategy)
        , unnest(cast(json_extract(strategy, '$.rules') as array<json>)) as t3(rule)
    where
        grass_date = date('2021-10-29')
        and event_name = 'mpcheckout'
        and user_id = 497131167         
    )
where
    cast(strategy_id as bigint) in (14000024, 14000025, 14000026, 14000027, 14000028, 14000029, 14000030, 14000031)
order by
    request_id
    , strategy_id
    , rule_id
	
	
Tabela nova