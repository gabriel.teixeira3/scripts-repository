SELECT DISTINCT buyer_id, buyer_shipping_address, buyer_shipping_address_city
, COUNT(distinct order_id) total_orders
, COUNT(( case when order_be_status_id in (2,4,11,12,13,14,15) then order_id else NULL end)) as orders_aprovadas
, CAST(("sum"(gmv)) AS decimal(20,2)) as valor_total_orders 
, CAST(("sum"(case when order_be_status_id in (2,4,11,12,13,14,15) then gmv else 0 end)) AS decimal(20,2)) as valor_orders_aprovadas
,COUNT(( case when order_be_status_id in (2,4,11,12,13,14,15) then order_id else NULL end)) * 100 / COUNT(distinct order_id) as percentual_aprovacao_orders
, CAST(("sum"(case when order_be_status_id in (2,4,11,12,13,14,15) then gmv else 0 end)) AS decimal(20,2)) * 100 / CAST(("sum"(gmv)) AS decimal(20,2)) as percentual_aprovacao_valor
FROM shopee_br.order_mart_dwd_order_all_event_final_status_df
--WHERE buyer_id = {{403413297}}
--where buyer_shipping_address like '%R Vicente S da Costa%'
WHERE buyer_shipping_address_zipcode IN ('06315010')
GROUP BY 1,2,3
