SELECT 

    COUNT(DISTINCT target_pk) as quantity_user_id,
    array_agg(DISTINCT target_pk) as users

FROM shopee_backend_log_br_db__shopee_audit_log_tab 
--WHERE json_extract_scalar(changes, '$.reason') LIKE '%Check 2: Buyer Registration and Order Time%'
WHERE json_extract_scalar(changes, '$.reason') LIKE  '%BR_54_cc_check1a%'
WHERE json_extract_scalar(changes, '$.reason') LIKE  '%BR_55_cc_check2a_%'
--WHERE json_extract_scalar(changes, '$.reason') LIKE  '%BR_56_cc_check3_%'
--WHERE json_extract_scalar(changes, '$.reason') LIKE  '%BR_57_cc_check1b_%'
--WHERE json_extract_scalar(changes, '$.reason') LIKE  '%BR_58_cc_check2b_%'
AND date(from_unixtime(action_time_msec/1000, 'America/Sao_Paulo')) = DATE('2021-05-18')
