SELECT DISTINCT shop_id
, COUNT(distinct order_id) gross_orders
, COUNT(( case when order_be_status_id in (2,4,11,12,13,14,15) then order_id else NULL end)) as net_orders
, CAST(("sum"(gmv)) AS decimal(20,2)) as gmv 
, CAST(("sum"(case when order_be_status_id in (2,4,11,12,13,14,15) then gmv else 0 end)) AS decimal(20,2)) as nmv
,COUNT(( case when order_be_status_id in (2,4,11,12,13,14,15) then order_id else NULL end)) * 100 / COUNT(distinct order_id) as net_orders_perc
, CAST(("sum"(case when order_be_status_id in (2,4,11,12,13,14,15) then gmv else 0 end)) AS decimal(20,2)) * 100 / CAST(("sum"(gmv)) AS decimal(20,2)) as nmv_perc_perc
FROM shopee_br.order_mart_dwd_order_all_event_final_status_df
WHERE create_datetime >= ('2021-03-01 00:00:0') AND < ('2021-04-30 23:59:59')
WHERE payment_be_channel = ('label_credit_card')
GROUP BY 1,2 
