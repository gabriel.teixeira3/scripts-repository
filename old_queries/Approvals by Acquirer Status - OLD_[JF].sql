WITH
base AS (
    SELECT DISTINCT 
        DATE_TRUNC('MONTH', DATE(CAST(t3.create_datetime AS TIMESTAMP))) transaction_month
        , DATE_TRUNC('WEEK', DATE(CAST(t3.create_datetime AS TIMESTAMP))) transaction_week
        , DATE(CAST(t3.create_datetime AS TIMESTAMP)) transaction_date
        , CASE WHEN acquirer_status like '%Accepted%' THEN 'Accepted'
            WHEN acquirer_status like '%Acquirer response error%' THEN 'Acquirer response error'
            WHEN acquirer_status like '%Cannot process transaction at this moment%' THEN 'Cannot process transaction at this moment'
            WHEN acquirer_status like '%Communication mismatch%' THEN 'Communication mismatch'
            WHEN acquirer_status like '%Expired card%' THEN 'Expired card'
            WHEN acquirer_status like '%High risk transaction%' THEN 'High risk transaction'
            WHEN acquirer_status like '%Inactive card%' THEN 'Inactive card'
            WHEN acquirer_status like '%Insufficient funds%' THEN 'Insufficient funds'
            WHEN acquirer_status like '%Internal server error%' THEN 'Internal server error'
            WHEN acquirer_status like '%Invalid card number%' THEN 'Invalid card number'
            WHEN acquirer_status like '%Invalid card or card type%' THEN 'Invalid card or card type'
            WHEN acquirer_status like '%Invalid due date%' THEN 'Invalid due date'
            WHEN acquirer_status like '%No response from acquirer%' THEN 'No response from acquirer'
            WHEN acquirer_status like '%Not accepted%' THEN 'Not accepted'
            WHEN acquirer_status like '%Security code mismatch%' THEN 'Security code mismatch'
            WHEN acquirer_status like '%Timeout%' THEN 'Timeout'
            WHEN acquirer_status like '%Transaction not found%' THEN 'Transaction not found'
            WHEN acquirer_status like '%Unknown acquirer error%' THEN 'Unknown acquirer error'
        END AS acquirer_status
        , CASE WHEN t3.payment_method_id = 38 THEN 'boleto'
            WHEN t3.payment_method_id = 1 THEN 'CC'
        END AS payment_method
        , CASE WHEN t3.pay_datetime is not null then 'CO' else NULL end as status
        , COUNT(DISTINCT t3.checkout_id) total_checkout
        , SUM(CAST(t3.gmv AS DECIMAL(20,2))) total_amount
    FROM shopee_br.order_mart_dwd_order_all_event_final_status_df as t3
        LEFT JOIN shopee_br.shopee_payment_module_payment_br_db__payment_v2_tab as t2 ON t3.payment_transaction_id = t2.transaction_id 
        LEFT JOIN shopee_br_anlys.ebanx_payments t1 on t1.hash = t2.channel_ref
    WHERE t2.channel_status <> 'PE'
        AND t2.channel_ref is not null
        AND t2.payment_type = 4
        AND DATE(CAST(t3.create_datetime AS TIMESTAMP)) >= DATE(current_timestamp at time zone 'America/Sao_Paulo' - interval '180' day)
    GROUP BY 1, 2, 3, 4, 5, 6
    ORDER BY 1 DESC, 2 DESC, 3 DESC
    ) 
SELECT DISTINCT 
    transaction_month
    , transaction_week
    , transaction_date
    , payment_method
    , acquirer_status
    , SUM(total_checkout) total_checkout
    , SUM(CASE WHEN status = 'CO' THEN total_checkout ELSE 0 END) total_approved
    , SUM(total_amount) total_amount
    , SUM(CASE WHEN status = 'CO' THEN total_amount ELSE 0 END) total_amount_approved
FROM base
GROUP BY 1, 2, 3, 4, 5
ORDER BY 1 DESC, 2 DESC, 3 DESC
