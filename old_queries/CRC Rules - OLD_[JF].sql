SELECT DISTINCT
date(FROM_UNIXTIME(ctime -39600)) day,
user_id,
case when u.status = 0 then 'deleted'
   when u.status = 1 then 'normal'
   when u.status = 2 then 'banned'
   when u.status = 3 then 'frozen'
   else null end as buyer_status,
strategy_name, 
strategy_id,
review_status,
t2.case_id,
t1.order_id,
checkout_id,
o.order_be_status,
--o.order_fe_status,
cast((gmv) as decimal(20,2)) gmv
FROM shopee_antifraud_case_review_center_db__mp_case_order_mapping_tab t1
left join order_mart_dwd_order_all_event_final_status_df o on o.order_id = t1.order_id
left join user_mart_dim_user u on u.user_id = o.buyer_id
inner join shopee_antifraud_case_review_center_db__mp_case_tab t2 on t2.case_id = t1.case_id
where t2.country ='BR' 
--and t2.strategy_name like '%CC Chargeback%'
and date(FROM_UNIXTIME(ctime -39600)) between date('2021-09-24') and date ('2021-09-30')
group by 1,2,3,4,5,6,7,8,9,10,11
