create table if not exists fba_fraud_user_entities as (

 WITH base as (
select    
               t2.buyer_id 
              ,t2.payment_transaction_id

              

    FROM  shopee_br.order_mart_dwd_order_all_event_final_status_df as t2 --ON t2.buyer_id  =  cast (ab.buyer_id as int)    
     
)
 
 -- TONGDUN --
 
  SELECT    userid,
  lower("to_hex"("from_base64"(hash_shopee_df))) record,
            'tongdun device' as entity

    FROM shopee_br.shopee_fraud_check_br_db__device_record_tab t1 
    INNER JOIN base as t2 on t2.buyer_id = t1.userid
        WHERE "to_hex"("from_base64"(hash_shopee_df)) <> ''

    UNION 
    
-- WEB DEVICE --

  SELECT    userid,
  lower(to_hex((hashed_fingerprint))) record,
            'web device' as entity
    FROM shopee_br.shopee_device_extension_db__device_ext_tab t1 
    INNER JOIN base as t2 on t2.buyer_id = t1.userid
        WHERE to_hex((hashed_fingerprint)) <> ''
        AND extinfo.is_web = True 
 
    UNION
    
-- ADDRESS --

  SELECT   
   t1.buyer_id
   
 , concat(lower(buyer_shipping_address),',',buyer_shipping_address_city) record,
            'address and city' as entity
       
    FROM shopee_br.order_mart_dwd_order_all_event_final_status_df t1 
    INNER JOIN base as t2 on t2.buyer_id = t1.buyer_id
        WHERE concat(lower(buyer_shipping_address),',',buyer_shipping_address_city) <> ''
  
    
    UNION
    
-- CC FINGERPRINT --

  SELECT    t1.user_id,
  identifier record,
            'cc identifier' as entity
    FROM shopee_br.shopee_payment_module_br_db__card_tab t1 
    INNER JOIN base as t2 on t2.buyer_id = t1.user_id
        WHERE identifier <> ''

    
    UNION 
    
-- PHONE --

  SELECT   t1.userid,
   phone record,
            'phone' as entity
    FROM (SELECT userid,
    			 phone
    	FROM shopee_br_s1.shopee_account_v2_db__account_tab

    UNION
       
        SELECT userid,
    		   shipping_phone phone
    	FROM shopee_br_s1.shopee_order_v4_db__order_v4_tab) t1 
    INNER JOIN base as t2 on t2.buyer_id = t1.userid
        WHERE phone <> ''
  

    UNION

-- EMAIL --

  SELECT    
  t1.userid,
  email record,
            'email' as entity
    FROM shopee_br_s1.shopee_account_v2_db__account_tab t1 
    INNER JOIN base as t2 on t2.buyer_id = t1.userid
        WHERE email <> ''



    UNION

-- CPF --
SELECT DISTINCT *

    FROM (
  SELECT DISTINCT
         t1.userid
       , cast(extinfo.tax_id as varchar) record
        ,'cpf' as entity

        FROM shopee_br_s1.shopee_account_v2_db__account_tab t1 
        INNER JOIN base as t2 on t2.buyer_id = t1.userid
        WHERE extinfo.tax_id  <> ''

    
        UNION
SELECT DISTINCT 
      t1.user_id
        , cast(json_extract(extra_data,'$.tax_id') as varchar) as record_
         ,'cpf' as entity

        FROM shopee_br.shopee_payment_module_transaction_br_db__transaction_v2_tab t1
        LEFT JOIN shopee_br.order_mart_dwd_order_all_event_final_status_df t2 on t2.payment_transaction_id = t1.transaction_id
        INNER JOIN base as t3 on t3.buyer_id = t1.user_id
   
   
    )
    WHERE record <> ''
)

