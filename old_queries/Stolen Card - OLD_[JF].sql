WITH fraud_tab as (
 SELECT DISTINCT

 t1.user_id, 
 date(cast(registration_datetime as TIMESTAMP))  registration_date, 
 "max_by"(t2.name, t3.ctime) as fraud_type,
 case when t1.status = 0 then 'deleted'
        when t1.status = 1 then 'normal'
        when t1.status = 2 then 'banned'
        when t1.status = 3 then 'frozen'
        else null end as buyer_status
    FROM shopee_br.user_mart_dim_user t1
    LEFT JOIN shopee_br.shopee_fraud_backend_br_db__fraud_user_tag_tab t3 ON (t1.user_id = t3.userid)
    LEFT JOIN shopee_br.shopee_fraud_backend_br_db__fraud_tag_tab t2 ON (t2.id = t3.tagid)
    WHERE ("date"(t1.grass_date) = "date"(current_timestamp AT TIME ZONE 'America/Sao_Paulo' - INTERVAL  '24' HOUR))
    group by 1,2,4
    )


SELECT 
    date(from_unixtime(action_time_msec/1000, 'America/Sao_Paulo')) action_time,
    target_pk as user_id,
    json_extract_scalar(changes, '$.status') status_change, 
    json_extract_scalar(changes, '$.reason') reason, 
    a.username,
    fraud_type,
    buyer_status
    
FROM shopee_backend_log_br_db__shopee_audit_log_tab  a
left join fraud_tab f on f.user_id = target_pk
WHERE target_tab = 'account_change_log'
    -- AND regexp_like(username, '@shopee') 
    -- AND regexp_like(changes, '(?i)fraud')
    AND date(from_unixtime(action_time_msec/1000, 'America/Sao_Paulo')) >= current_date - interval '30' day -- adjust the period here
    and fraud_type = 'Stolen Card'
