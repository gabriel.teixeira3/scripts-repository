SELECT DISTINCT buyer_id
, "count"(DISTINCT order_id) gross_orders
, "count"((case when order_be_status_id in (2,4,11,12,13,14,15) then order_id else 0 end)) as net_orders
, CAST(("sum"(gmv)) AS decimal(20,2)) as gmv 
, CAST(("sum"(case when order_be_status_id in (2,4,11,12,13,14,15) then gmv else 0 end)) AS decimal(20,2)) as nmv
FROM shopee_br.order_mart_dwd_order_all_event_final_status_df
WHERE buyer_id in (193466081,
197938243,
199468542,
199794236,
199914813,
200346415)
GROUP BY 1
