WITH tax_id as (
SELECT transaction_id, (json_extract(extra_data,'$.tax_id')) as tax_id
from shopee_payment_module_transaction_br_db__transaction_tab t),

telephone as (
select phone, email, userid
FROM  shopee_br_s1.shopee_account_v2_db__account_tab),

cardhorlder AS (SELECT 
checkout_id,
cardholder,
card_number,
card_bank
from shopee_br_anlys.ebanx_payments p 
LEFT JOIN shopee_payment_module_payment_br_db__payment_v2_tab c on c.channel_ref = p.hash
LEFT JOIN order_mart_dwd_order_all_event_final_status_df o on o.payment_transaction_id = c.transaction_id)

Select  cc.checkout_id,
tax_id,
cardholder,
phone,
email,
card_number,
card_bank,
cast((gmv) as decimal(20,2)) gmv


from order_mart_dwd_order_item_all_event_final_status_df o
left join tax_id t on t.transaction_id = o.payment_transaction_id
left join telephone tel on tel.userid = o.buyer_id
left join cardhorlder cc on cc.checkout_id = o.checkout_id

where cc.checkout_id in ()
GROUP BY 1,2,3,4,5,6,7,8
