SELECT buyer_id,
buyer_shipping_address,
buyer_shipping_address_city,
buyer_shipping_address_state,
checkout_id,
array_agg(distinct order_id) orders,
amount_chargeback, 
description
FROM shopee_payment_module_payment_br_db__payment_v2_tab p
LEFT JOIN shopee_checkout_v2_db__checkout_v2_tab c on c.checkout_info.checkout_payment_info.transaction_id = transaction_id
LEFT JOIN shopee_br_bi_team__chargeback_ebanx e on p.channel_ref = e.pmt_hash
LEFT JOIN order_mart_dwd_order_all_event_final_status_df o on o.checkout_id = c.checkoutid
--WHERE
--(buyer_id =  ({{buyer_id}})
--or checkout_id = ({{checkout_id}})
--or buyer_shipping_address = ({{buyer_shipping_address}}))
