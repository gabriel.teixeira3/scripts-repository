SELECT DISTINCT
  t1.user_id
  , date(cast(registration_datetime as TIMESTAMP))  registration_date
  , "max_by"(t2.name, t3.ctime) as fraud_type
  ,case when t1.status = 0 then 'deleted'
   when t1.status = 1 then 'normal'
   when t1.status = 2 then 'banned'
   when t1.status = 3 then 'frozen'
   else null end as buyer_status
   FROM shopee_br.user_mart_dim_user t1
  LEFT JOIN shopee_br.shopee_fraud_backend_br_db__fraud_user_tag_tab t3 ON (t1.user_id = t3.userid)
 LEFT JOIN shopee_br.shopee_fraud_backend_br_db__fraud_tag_tab t2 ON (t2.id = t3.tagid)
 WHERE user_id IN (403413297,423440803,279670299)
group by 1,2,4
