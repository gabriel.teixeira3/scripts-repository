with
checkout_ref as (
    select distinct
        c.checkoutid checkout_id
        , c.checkout_info.checkout_payment_info.transaction_id transaction_id
        , channel_ref
        , "max_by"(op.is_cb_shop, op.gmv) is_cross_border
    from shopee_br.shopee_checkout_v2_db__checkout_v2_tab c
        left join shopee_br.order_mart_dwd_order_all_event_final_status_df op ON (c.checkoutid = op.checkout_id)
        left join shopee_br.shopee_payment_module_payment_br_db__payment_v2_tab pay on op.payment_transaction_id = pay.transaction_id
    where c.country = 'BR'
        and date(cast(create_datetime as TIMESTAMP)) >= date(current_timestamp at time zone 'America/Sao_Paulo' - interval '3' month)
    group by 1, 2, 3
    )
, ckt_order_tab as (
    select distinct
        checkout_id
        , payment_transaction_id
        , date(date_parse(create_datetime, '%Y-%m-%d %H:%i:%s')) date_attempt
        , case when sum(gmv) < 100 then '< 100 BRL'
            when sum(gmv) between 100 and 300 then '100 - 300 BRL'
            when sum(gmv) between 300 and 600 then '300 - 600 BRL'
            when sum(gmv) > 600 then '> 600 BRL' 
        else null end price_range
        , count(order_id) as orders, sum(gmv_usd) as gmv_usd
    from shopee_br.order_mart_dwd_order_all_event_final_status_df o
        inner join shopee_br.shopee_payment_module_payment_br_db__payment_v2_tab ct on o.payment_transaction_id = ct.transaction_id
    where grass_region = 'BR' 
        and payment_type = 4
        and date(cast(create_datetime as TIMESTAMP)) >= date(current_timestamp at time zone 'America/Sao_Paulo' - interval '3' month)
    group by 1,2,3
    )
, case_info as (
    select distinct 
        a.order_id
        , o.checkout_id
        , o.gmv
        , o.price_range
        , o.date_attempt
        , operator
        , case when max(operation) = 2 and max(b.cancel_result) = 1 then 'CRC ALL cancel success'
            when max(operation) = 2 and max(b.cancel_result) = 2 then 'CRC ALL cancel failed'
            when max(operation) = 2 and max(b.cancel_result) = 3 then 'CRC ALL cancel pending'
            when max(operation) = 2 and max(b.cancel_result) = 4 then 'CRC ALL cancel already cancelled'
            when count(case when operator = 'SYSTEM' then null else a.order_id end) > 0 and max(o.buyer_cancel_reason) is null then 'CRC ALL dismissed'
            when count(case when operator = 'SYSTEM' then null else a.order_id end) > 0 and max(o.buyer_cancel_reason) is not null then 'CRC ALL buyer cancel'
        else 'CRC ALL expired' end as strategy
    from shopee_br.shopee_antifraud_case_review_center_db__mp_order_action_log_tab a
        left join (
            select distinct 
                order_id
                , cancel_result
            from shopee_br.shopee_antifraud_case_review_center_db__mp_order_action_log_tab
            where operation = 2 
                and country = 'BR'
                ) b on a.order_id = b.order_id
        inner join (
            select distinct
                om.checkout_id
                , om.order_id
                , om.gmv
                , om.buyer_cancel_reason
                , o.price_range
                , o.date_attempt
            from shopee_br.order_mart_dwd_order_all_event_final_status_df om
                inner join ckt_order_tab o on o.checkout_id = om.checkout_id
            where grass_region = 'BR' 
                and date(cast(create_datetime as TIMESTAMP)) >= date(current_timestamp at time zone 'America/Sao_Paulo' - interval '3' month)
                ) o on a.order_id = o.order_id
    where country = 'BR' 
    group by 1, 2, 3, 4, 5, 6
    )
, crc_all as (
    select distinct
        date_trunc('month', date_attempt) create_month
        , date_trunc('week', date_attempt) create_week
        --, date_attempt create_date 
        , strategy 
        , price_range
        --, operator
        , count(distinct checkout_id) as attempts
        , count(distinct order_id) as orders
        , sum(gmv) as gmv_brl
    from case_info
    group by 1, 2, 3, 4--, 5, 6
    )
, crc_paid as (
    select distinct
        date_trunc('month', date_attempt) create_month
        , date_trunc('week', date_attempt) create_week
        --, date_attempt create_date 
        , replace(strategy,'ALL','PAID') as strategy
        , price_range
        --, operator
        , count(distinct checkout_id) as attempts
        , count(distinct order_id) as orders
        , sum(gmv) as gmv_brl
    from case_info crc
        inner join shopee_br.shopee_checkout_v2_db__checkout_v2_tab co on co.checkoutid = crc.checkout_id
    where from_unixtime(co.ctime - 11*3600) >= date(current_timestamp at time zone 'America/Sao_Paulo' - interval '3' month) 
        and co.status in (4,5)
    group by 1, 2, 3, 4--, 5, 6
    )
, crc_cbk as (
    select distinct
        date_trunc('month', date_attempt) create_month
        , date_trunc('week', date_attempt) create_week
        --, date_attempt create_date 
        , replace(strategy,'ALL','CBK') as strategy
        , price_range
        --, operator
        , count(distinct crc.checkout_id) as attempts
        , count(distinct order_id) as orders
        , sum(gmv) as gmv_brl
    from case_info crc
        inner join checkout_ref ref on ref.checkout_id = crc.checkout_id
        inner join shopee_br_anlys.ebanx_chargebacks cb on cb.pmt_hash = ref.channel_ref
    group by 1, 2, 3, 4--, 5, 6
    )
select * from crc_all UNION
select * from crc_paid UNION
select * from crc_cbk
