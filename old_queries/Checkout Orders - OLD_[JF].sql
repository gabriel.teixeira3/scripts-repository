ckt_order_tab as (
    select checkout_id, payment_transaction_id,
           date(date_parse(create_datetime, '%Y-%m-%d %H:%i:%s')) date_attempt,
           (case when sum(gmv) < 100 then '< 100 BRL'
                 when sum(gmv) between 100 and 300 then '100 - 300 BRL'
                 when sum(gmv) between 300 and 600 then '300 - 600 BRL'
                 when sum(gmv) > 600 then '> 600 BRL' else null end) price_range,
           count(order_id) as orders, sum(gmv_usd) as gmv_usd
    from shopee.order_mart_dwd_order_all_event_final_status_df o
    inner join shopee.shopee_regional_op_team__payment_channel_tab ct on cast(ct.be_channel_id as int) = o.payment_be_channel_id
    where grass_region = 'BR' and ct.reg_channel_parent = 'Card Payment Overall' and ct.country = 'BR'
    and date_parse(create_datetime, '%Y-%m-%d %H:%i:%s') >= date '2021-01-01'
    and date_parse(create_datetime, '%Y-%m-%d %H:%i:%s') < current_date - interval '1' day
    group by 1,2,3
),

gross_ckt as (
    select date_attempt, 'Total Checkouts' as strategy, price_range,
           count(distinct checkout_id) as attempts, sum(orders) as orders, sum(gmv_usd) as gmv_usd
    from ckt_order_tab
    group by 1,2,3
),

transaction as (
    select pmt.transaction_id, 
        (case when count(case when status = 20 then payment_id else null end) > 0 then 'success'
                when count(case when status in (0,8) then payment_id else null end) > 0 then 'pending'
                when count(case when status = 22 then payment_id else null end) > 0 then 'failed'
                when count(case when status in (26,100) then payment_id else null end) > 0 then 'spm'
                when count(case when status in (50,51) then payment_id else null end) > 0 then 'cancelled'
                when count(case when status in (2,54) then payment_id else null end) > 0 then 'expired'
                else 'error' end) as status,
        (case when max_ebanx is null then 'no'       when max_issuer is null then 'yes'
                when max_ebanx > max_issuer then 'yes' else 'no' end) as ebanx_blocked,
        (case when max_issuer is null then 'no'      when max_ebanx is null then 'yes'
                when max_issuer > max_ebanx then 'yes' else 'no' end) as issuer_reject
    from shopee_sensitive_extra.shopee_payment_module_payment_br_db__payment_v2_tab pmt
    left join (
        SELECT transaction_id, max(ctime) max_ebanx
        from shopee_sensitive_extra.shopee_payment_module_payment_br_db__payment_v2_tab
        where payment_type = 4 and status = 22
        and COALESCE(json_extract_scalar(extra_data,'$.pay_result.payment.transaction_status.description'),
                    json_extract_scalar(extra_data,'$.query_result.payment.transaction_status.description'),
                    json_extract_scalar(extra_data,'$.pay_response.transaction_status.description')
                    ) LIKE '%High risk transaction%'
        group by 1
    ) a on a.transaction_id = pmt.transaction_id
    left join (
        SELECT transaction_id, max(ctime) max_issuer
        from shopee_sensitive_extra.shopee_payment_module_payment_br_db__payment_v2_tab
        where payment_type = 4 and status = 22
        and COALESCE(json_extract_scalar(extra_data,'$.pay_result.payment.transaction_status.description'),
                    json_extract_scalar(extra_data,'$.query_result.payment.transaction_status.description'),
                    json_extract_scalar(extra_data,'$.pay_response.transaction_status.description')
                    ) in ('Not accepted', 'Invalid card or card type', 'Insufficient funds',
                        'Security code mismatch', 'Invalid card number', 'Expired card', 'Inactive card')
        group by 1
    ) b on b.transaction_id = pmt.transaction_id
    where payment_type = 4 and json_extract_scalar(extra_data, '$.provision_channel_id') = '10000101' --shopee ckt
    and from_unixtime(ctime - 11*3600) >= date '2021-01-01'
    and from_unixtime(ctime - 11*3600) < current_date - interval '1' day
    group by 1,3,4
),

not_spm as (
    select date_attempt, price_range,
           count(distinct checkout_id) as attempts, sum(orders) as orders, sum(gmv_usd) as gmv_usd
    from transaction txn
    inner join ckt_order_tab o on txn.transaction_id = o.payment_transaction_id
    where txn.status != 'spm'
    group by 1, 2
),

spm as (
    select a.date_attempt,
           'SPM Rule' as strategy,
           a.price_range,
           b.attempts - a.attempts as attempts,
           b.orders - a.orders as orders,
           b.gmv_usd - a.gmv_usd as gmv_usd
    from not_spm a
    left join gross_ckt b on a.date_attempt = b.date_attempt and a.price_range = b.price_range
),

ebanx as (
    select date_attempt, (case when ebanx_blocked = 'yes' then 'Ebanx reject'
                               when issuer_reject = 'yes' then 'Issuer reject'
                               else 'Expired Checkouts' end) as strategy, price_range, --error as expired
           count(distinct checkout_id) as attempts, sum(orders) as orders, sum(gmv_usd) as gmv_usd
    from transaction txn
    inner join ckt_order_tab o on txn.transaction_id = o.payment_transaction_id
    where txn.status = 'failed'
    group by 1, 2, 3
),

other as (
    select date_attempt, (case when status = 'pending' then 'Pending Checkouts'
                               when status = 'cancelled' then 'Cancelled Checkouts'
                               when status = 'expired' then 'Expired Checkouts'
                               else 'Expired Checkouts' end) as strategy, price_range, --error as expired
           count(distinct checkout_id) as attempts, sum(orders) as orders, sum(gmv_usd) as gmv_usd
    from transaction txn
    inner join ckt_order_tab o on txn.transaction_id = o.payment_transaction_id
    where txn.status in ('pending', 'cancelled', 'expired', 'error')
    group by 1, 2, 3
),

paid_ckt as (
    select date_attempt, 'Paid Checkouts' as strategy, price_range,
           count(distinct checkout_id) as attempts, sum(orders) as orders, sum(gmv_usd) as gmv_usd
    from shopee.shopee_checkout_v2_db__checkout_v2_tab ckt
    inner join ckt_order_tab o on ckt.checkoutid = o.checkout_id
    where grass_region = 'BR' and status in (4,5)
    and from_unixtime(ctime - 11*3600) >= date '2021-01-01'
    group by 1, 2, 3
),

crc as (
    SELECT date(date_parse(create_datetime, '%Y-%m-%d %H:%i:%s')) as date_attempt, 'CRC' as strategy, price_range,
           count(distinct o.checkout_id) as attempts, count(distinct o.order_id) as orders, sum(o.gmv_usd) as gmv_usd 
    from shopee.shopee_antifraud_case_review_center_db__mp_order_action_log_tab crc
    inner join shopee.order_mart_dwd_order_all_event_final_status_df o on crc.order_id = o.order_id
    inner join (select distinct checkout_id, price_range from ckt_order_tab) c on o.checkout_id = c.checkout_id
    where crc.country = 'BR' and crc.cancel_result = 1 and o.grass_region = 'BR'
    and date_parse(create_datetime, '%Y-%m-%d %H:%i:%s') >= date '2021-01-01'
    and date_parse(create_datetime, '%Y-%m-%d %H:%i:%s') < current_date - interval '1' day
    group by 1,2,3
),

cbk_date as (
    select date(date_parse(substr(chargeback_date,1,10),'%Y-%m-%d')) date_attempt, 'Chargeback by cbk date' as strategy, price_range,
           count(distinct checkout_id) as attempts, sum(orders) as orders, sum(gmv_usd) as gmv_usd
    from shopee.shopee_regional_op_team__payments_br_chargeback_all cb
    inner join shopee_sensitive_extra.shopee_payment_module_payment_br_db__payment_v2_tab pmt on pmt.channel_ref = cb.pmt_hash
    inner join ckt_order_tab o on pmt.transaction_id = o.payment_transaction_id --should -120 days if want to get accurate data for jan
    and from_unixtime(pmt.ctime - 11*3600) >= date '2021-01-01'
    and date(date_parse(substr(chargeback_date,1,10),'%Y-%m-%d')) < current_date - interval '1' day
    and date(date_parse(substr(chargeback_date,1,10),'%Y-%m-%d')) >= date '2021-01-01'
    group by 1, 2, 3
),

cbk as (
    select date_attempt, 'Chargeback' as strategy, price_range,
           count(distinct checkout_id) as attempts, sum(orders) as orders, sum(gmv_usd) as gmv_usd
    from shopee.shopee_regional_op_team__payments_br_chargeback_all cb
    inner join shopee_sensitive_extra.shopee_payment_module_payment_br_db__payment_v2_tab pmt on pmt.channel_ref = cb.pmt_hash
    inner join ckt_order_tab o on pmt.transaction_id = o.payment_transaction_id
    and from_unixtime(pmt.ctime - 11*3600) >= date '2021-01-01'
    group by 1, 2, 3
)

select * from evt_all UNION
select * from ds_online UNION
select * from gross_ckt UNION
select * from spm UNION
select * from ebanx UNION
select * from other UNION
select * from paid_ckt UNION
select * from crc UNION
select * from cbk_date UNION
select * from cbk
