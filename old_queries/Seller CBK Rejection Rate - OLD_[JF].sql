WITH 
order_base AS (
    SELECT 
        t.payment_transaction_id as tnx_id,
        checkoutid,
        seller_id,
        arbitrary(date(split(t.create_datetime, ' ')[1])) order_date,
        count(distinct order_id) no_order, 
        max(case when t2.pmt_hash is not null then 1 else 0 end) is_chargeback,
        max(t2.amount_chargeback) amount_chargeback, 
        arbitrary(t2.chargeback_date) chargeback_date,
        sum(gmv) gmv
    FROM shopee_br.order_mart_dwd_order_all_event_final_status_df t
        JOIN (
            SELECT 
                transaction_id,
                max(date(from_unixtime(ctime, 'America/Sao_Paulo'))) payment_cdate, 
                max_by(channel_ref, case when channel_ref is not null then ctime end) channel_ref
            FROM shopeepay.shopee_payment_module_payment_br_db__payment_v2_tab__br_continuous_s0_live
            GROUP BY 1 
        ) t1 ON t1.transaction_id = t.payment_transaction_id
        
        LEFT JOIN (
            SELECT 
                pmt_hash,
                max_by(date(chargeback_date), ingestion_timestamp) chargeback_date,
                max_by(cast(amount_chargeback as double), ingestion_timestamp) amount_chargeback
            FROM shopee_br.shopee_br_bi_team__chargeback_ebanx 
            where  date(chargeback_date) between date('2021-04-01') and date('2021-04-30')
            GROUP BY 1 
        ) t2 ON t2.pmt_hash = t1.channel_ref 
        
        JOIN (
            SELECT 
                c.checkoutid, 
                substr(cast(json_extract(json_extract(checkout_info.checkout_payment_info.channel_item_option_info, '$.credit_card_data'), '$.card_number') AS VARCHAR), 1, 6) bin_number,
                cast(json_extract(json_extract(checkout_info.checkout_payment_info.channel_item_option_info, '$.credit_card_data'), '$.card_number') AS VARCHAR) _6x4_number,
                cast(json_extract(json_extract(checkout_info.checkout_payment_info.channel_item_option_info, '$.credit_card_data'), '$.bank_name') AS VARCHAR) bank_name 
            FROM shopee_br.shopee_checkout_v2_db__checkout_v2_tab c
        ) t3 ON t3.checkoutid = t.checkout_id
    WHERE t.grass_region = 'BR'
        AND t.payment_be_channel LIKE '%label_credit_card%'
        AND date(split(create_datetime, ' ')[1]) between date('2021-03-01') and date('2021-04-30')
        
    GROUP BY 1,2,3
)
,seller_rate as (
SELECT DISTINCT
seller_id
, COUNT(distinct order_id) gross_orders
, COUNT(( case when order_be_status_id in (2,4,11,12,13,14,15) then order_id else NULL end)) as net_orders
, CAST(("sum"(gmv)) AS decimal(20,2)) as gmv 
, CAST(("sum"(case when order_be_status_id in (2,4,11,12,13,14,15) then gmv else 0 end)) AS decimal(20,2)) as nmv
,COUNT(( case when order_be_status_id in (2,4,11,12,13,14,15) then order_id else NULL end)) * 100 / COUNT(distinct order_id) as net_orders_perc
, CAST(("sum"(case when order_be_status_id in (2,4,11,12,13,14,15) then gmv else 0 end)) AS decimal(20,2)) * 100 / CAST(("sum"(gmv)) AS decimal(20,2)) as nmv_perc_perc
FROM shopee_br.order_mart_dwd_order_all_event_final_status_df
WHERE payment_be_channel = 'label_credit_card' 
GROUP BY 1
HAVING CAST(("sum"(gmv)) AS decimal(20,2)) >0
)

SELECT
is_chargeback,
CASE WHEN seller_rate.gmv = 0 then 0 else ((seller_rate.gmv-seller_rate.nmv)/seller_rate.gmv) end as  rejection_rate,
count(distinct tnx_id) count_chk,
sum(amount_chargeback) amount_chargeback


FROM order_base
LEFT JOIN seller_rate on seller_rate.seller_id = order_base.seller_id
GROUP BY 1,2

