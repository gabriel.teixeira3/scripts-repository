select distinct  o_cbk.payment_transaction_id
                ,o_cbk.buyer_id
                ,o_cbk.gmv as amount_cbk
                ,date((lpad(cbk_cbk.chargeback_date,10,'0'))) as chargeback_date
                ,date(o_cbk.create_datetime) as order_date
                ,substring(replace(replace(replace(card.card_number,'*',''),' ',''),'.0',''),1,6) as bin 
                ,substring(replace(replace(replace(card.card_number,'*',''),' ',''),'.0',''),7,4) as final
                ,cbk_cbk.credit_card_brand
                from shopee_br.order_mart_dwd_order_all_event_final_status_df o_cbk 
                inner join shopee_br.shopee_payment_module_payment_br_db__payment_v2_tab payment_cbk on payment_cbk.transaction_id = o_cbk.payment_transaction_id 
                inner join shopee_br.shopee_br_bi_team__ebanx_payments_v3  card on card.hash = payment_cbk.channel_ref
                inner join shopee_br.shopee_br_bi_team__chargeback_ebanx cbk_cbk on cbk_cbk.pmt_hash = payment_cbk.channel_ref
                where o_cbk.payment_method_id = 1
                and o_cbk.order_be_status_id in(2, 4, 11, 12, 13, 14, 15)
                and date(cbk_cbk.chargeback_date) >= (current_date - interval '30' day)