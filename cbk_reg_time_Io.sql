WITH cbk AS (
SELECT DISTINCT
	buyer_id,
		transaction_id,
		(from_unixtime(ctime, 'America/Sao_Paulo')) payment_cdate,
		chargeback_date,
		amount_chargeback,
		CASE 
			WHEN t2.pmt_hash IS NOT NULL THEN 1
			ELSE NULL
		END AS is_chargeback
FROM
	shopee_payment_module_payment_br_db__payment_v2_tab AS t1
		LEFT JOIN order_mart_dwd_order_all_event_final_status_df AS o on o.payment_transaction_id = t1.transaction_id
		INNER JOIN (
			SELECT DISTINCT
				pmt_hash,
					MAX_BY(DATE(CAST(chargeback_date AS TIMESTAMP)), ingestion_timestamp) AS chargeback_date,
					MAX_BY(CAST(amount_chargeback AS DOUBLE), ingestion_timestamp) AS amount_chargeback
			FROM
				shopee_br_bi_team__chargeback_ebanx
			WHERE
				LOWER(description) LIKE '%fraud%'
			GROUP BY
				1
		) AS t2 ON t2.pmt_hash = t1.channel_ref
WHERE 
	t1.channel_ref IS NOT NULL
		AND o.payment_be_channel LIKE '%label_credit_card%'
		AND date(CAST(o.create_datetime AS timestamp)) >= (CURRENT_DATE - INTERVAL '30' DAY)
),

request_time AS (
SELECT
	p1.buyer_id,
		checkout_id,
		date_diff('hour', CAST(p2.registration_datetime AS TIMESTAMP), CAST(p1.create_datetime AS TIMESTAMP)) AS request_diff,
		CASE 
			WHEN date_diff('hour',CAST(p2.registration_datetime AS TIMESTAMP), CAST(p1.create_datetime AS TIMESTAMP)) <= 6 THEN 'fraud'
			ELSE NULL 
		END AS request_diff_time
FROM (
	SELECT 
		buyer_id,
			checkout_id,
			MIN(create_datetime) AS create_datetime 
	FROM 
		order_mart_dwd_order_all_event_final_status_df
	GROUP BY 
		1,2
) AS p1
		LEFT JOIN user_mart_dim_user AS p2 ON p2.user_id = p1.buyer_id
WHERE
	DATE(CAST(p1.create_datetime AS TIMESTAMP)) >= (CURRENT_DATE - INTERVAL '30' DAY)
		--AND --between date('2021-07-05') AND date('2021-07-26')
GROUP BY 
	1,2,3
),

final_base AS (
	SELECT
		COUNT(is_chargeback) AS no_cbk,
			CAST(SUM(amount_chargeback) AS decimal (20,0)) AS cbk_gmv
	FROM 
		cbk AS c
			INNER JOIN request_time rq ON rq.buyer_id = c.buyer_id
	WHERE 
		request_diff_time LIKE 'fraud'
			AND is_chargeback = 1
)

SELECT 
	* 
FROM
	final_base