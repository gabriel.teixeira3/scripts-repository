-- WITH cbk_base AS (
--     SELECT DISTINCT
--         pmt_hash
--         , date(CAST(chargeback_date AS TIMESTAMP)) chargeback_date
--         , CAST(amount_chargeback AS DOUBLE) amount_chargeback
--     FROM shopee_br_anlys.ebanx_chargebacks
--     WHERE lower(description) like '%fraud%' 
--     )
SELECT DISTINCT
    t3.buyer_id
    --, t2.transaction_id
    , t2.payment_id
    , t1.card_bank
    , DAY(from_unixtime(t2.ctime,'America/Sao_Paulo')) as day -- data do payments
    , HOUR(from_unixtime(t2.ctime,'America/Sao_Paulo')) as hour-- data do payments
    , from_unixtime(t2.ctime, 'America/Sao_Paulo') payment_ctime -- data do payments
    , case when t1.acquirer_status like '- Accepted' then 'Accepted'
         when t1.acquirer_status like '0 - Accepted' then 'Accepted'
         when t1.acquirer_status like 'Accepted' then 'Accepted'
         when t1.acquirer_status like '%High risk transaction%' then 'Rejected'
    else 'Rejected' end as order_status
    , case when t5.status = 0 then 'deleted'
   when t5.status = 1 then 'normal'
   when t5.status = 2 then 'banned'
   when t5.status = 3 then 'frozen'
   else null end as buyer_status,
  t3.buyer_shipping_address_state,
  t3.buyer_shipping_address_city,
  t3.buyer_shipping_address,
   t1.amount
 
   -- t6.amount_chargeback as amount_cbk
  
    -- t6.chargeback_date
FROM shopee_br.shopee_payment_module_payment_br_db__payment_v2_tab as t2  --tabela de pagamentos
    LEFT JOIN shopee_br_anlys.ebanx_payments as t1 ON t1.hash  = t2.channel_ref  -- tabela EBANX 
    LEFT JOIN  shopee_br.order_mart_dwd_order_all_event_final_status_df as t3 ON t3.payment_transaction_id = t2.transaction_id -- tabela de order    
    LEFT JOIN  shopee_br.user_mart_dim_user as t5 on t5.user_id = t3.buyer_id --tabela user
    LEFT JOIN shopee_br_sensitive_extra.shopee_payment_module_payment_br_db__payment_tab as t9 on t9.transaction_id = t2.transaction_id
   
    --LEFT JOIN cbk_base t6 ON t6.pmt_hash = t2.channel_ref -- subquery cbk
WHERE t2.channel_ref is not null
    AND t2.payment_type = 4
    AND t3.grass_region = 'BR'
  
   -- AND date(CAST(chargeback_date AS timestamp)) between date('2021-07-01') and date('2021-08-30') or date(CAST(chargeback_date AS timestamp)) is NULL -- data do CBK 
    AND date(from_unixtime(t2.ctime, 'America/Sao_Paulo')) between date('2021-09-07') and date('2021-09-27') -- Data do pagamento
    AND t3.payment_be_channel LIKE '%label_credit_card%'
    AND t1.card_bank is  NOT NULL
    AND date(t5.grass_date) = date(current_timestamp at time zone 'America/Sao_Paulo' - interval '24' hour)

