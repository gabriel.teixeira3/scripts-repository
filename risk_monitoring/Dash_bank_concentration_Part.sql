SELECT DISTINCT
--         pmt_hash
--         , date(CAST(chargeback_date AS TIMESTAMP)) chargeback_date
--         , CAST(amount_chargeback AS DOUBLE) amount_chargeback
--     FROM shopee_br_anlys.ebanx_chargebacks
--     WHERE description IN ('Fraud (card-not-present)/No cardholder authorisation'
--         ,'Instalment dispute / Fraudulent Multiple Transactions'
--         , 'Fraud (card-not-present)/No cardholder authorization'
--         , 'Transaction not recognised'
--         )
--     )
SELECT DISTINCT
  DATE ( from_unixtime(t2.ctime, 'America/Sao_Paulo'))payment_creation_date --criação do pagamento
--    ,t6.chargeback_date --criação do chargeback
 ,HOUR(from_unixtime(t2.ctime,'America/Sao_Paulo')) as hour-- data do payments
    ,DAY(from_unixtime(t2.ctime,'America/Sao_Paulo')) as day 
    , CASE WHEN t1.card_bank IN ('NU PAGAMENTOS, S.A.', 'NUBANK') THEN 'NUBANK'
        WHEN t1.card_bank LIKE '%ITAU%' THEN 'ITAU'
        WHEN t1.card_bank LIKE '%SANTANDER%' THEN 'SANTANDER'
        WHEN t1.card_bank LIKE '%BRADESCO%' THEN 'BRADESCO'
        WHEN t1.card_bank LIKE '%NEON%' THEN 'NEON'
        WHEN t1.card_bank LIKE '%BANCO DO BRASIL S.A.%' THEN 'BANCO DO BRASIL'
        WHEN t1.card_bank LIKE '%C6%' THEN 'C6'
        WHEN t1.card_bank LIKE '%CAIXA ECONOMICA FEDERAL%' THEN 'CAIXA ECONOMICA FEDERAL'
        WHEN t1.card_bank LIKE '%BANCO INTER S.A.%' THEN 'BANCO INTER S.A.'
    ELSE 'Other banks' END AS card_bank --banco das transações
    , COUNT(DISTINCT t3.buyer_id) total_buyer -- total de buyers (mesma data, mesmo banco)
    , COUNT(DISTINCT t3.seller_id) total_seller -- total de sellers (mesma data, mesmo banco)
    , COUNT(DISTINCT t2.payment_id) total_transactions -- total de transações (mesma data, mesmo banco)
    , COUNT(DISTINCT CASE WHEN t1.acquirer_status like '%Accepted%' THEN t2.payment_id END) total_accepted_transaction -- total de transações aprovadas (mesma data, mesmo banco)
   -- , COUNT(DISTINCT CASE WHEN t1.acquirer_status like '%High risk transaction%' THEN t2.payment_id END) total_fraud_rejected_transaction -- total de transações reprovadas por fraud (mesma data, mesmo banco)
    , (COUNT(DISTINCT t2.payment_id) - (COUNT(DISTINCT CASE WHEN t1.acquirer_status like '%Accepted%' THEN t2.payment_id END) + COUNT(DISTINCT CASE WHEN t1.acquirer_status like '%High risk transaction%' THEN t2.payment_id END))) total_other_reasons_rejected_transaction -- total de transações reprovadas exceto fraud (mesma data, mesmo banco)
  --  , COUNT(DISTINCT t6.pmt_hash) as total_cbk -- total de transações com chargeback
  --  , SUM(t6.amount_chargeback) as total_amount_chargeback -- soma total de chargeback
  
FROM shopee_br.shopee_payment_module_payment_br_db__payment_v2_tab as t2  --tabela de pagamentos
    LEFT JOIN shopee_br_anlys.ebanx_payments as t1 ON t1.hash  = t2.channel_ref  -- tabela EBANX 
    LEFT JOIN  shopee_br.order_mart_dwd_order_all_event_final_status_df as t3 ON t3.payment_transaction_id = t2.transaction_id -- tabela de order    
    LEFT JOIN  shopee_br.user_mart_dim_user as t5 on t5.user_id = t3.buyer_id --tabela user
 --   LEFT JOIN cbk_base t6 ON t6.pmt_hash = t2.channel_ref -- subquery cbk
WHERE t2.channel_ref is not null
    AND t2.payment_type = 4
    AND t3.grass_region = 'BR'
    AND t1.card_bank is NOT NULL
    AND date(t5.grass_date) = date(current_timestamp at time zone 'America/Sao_Paulo' - interval '24' hour)
    AND date(from_unixtime(t2.ctime, 'America/Sao_Paulo')) between date('2021-09-01') and date('2021-09-27') 
   GROUP BY 1, 2,3,4
