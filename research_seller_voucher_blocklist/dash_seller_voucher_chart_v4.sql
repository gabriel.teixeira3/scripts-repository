WITH asf_final AS (
	SELECT *
	FROM 
		shopee_br_anlys.asf_orders
),

fraud_tag AS (
    SELECT DISTINCT
        t1.user_id,
            DATE(CAST(registration_datetime AS TIMESTAMP)) AS registration_date,
            MAX_BY(t2.name, t3.ctime) AS fraud_type,
            CASE   
                WHEN t1.status = 0 THEN 'deleted'
                WHEN t1.status = 1 THEN 'normal'
                WHEN t1.status = 2 THEN 'banned'
                WHEN t1.status = 3 THEN 'frozen'
                ELSE NULL 
            END AS user_status
    FROM 
        shopee_br.user_mart_dim_user AS t1
            LEFT JOIN shopee_br.shopee_fraud_backend_br_db__fraud_user_tag_tab AS t3 ON (t3.userid = t1.user_id)
            LEFT JOIN shopee_br.shopee_fraud_backend_br_db__fraud_tag_tab AS t2 ON (t2.id = t3.tagid)
    WHERE 
        (DATE(t1.grass_date) = DATE(CURRENT_TIMESTAMP AT TIME ZONE 'America/Sao_Paulo' - INTERVAL '48' HOUR)) 
            AND t3.status = 1
    GROUP BY 
        1,2,4
),

orders AS (
    SELECT DISTINCT
        DATE(CAST(FROM_UNIXTIME(t1.create_timestamp) AT TIME ZONE 'America/Sao_Paulo' AS DATE)) AS date,
            COUNT(DISTINCT t1.order_id) AS total_orders,
            COUNT(DISTINCT t1.seller_id) AS total_sellers,
            COUNT(DISTINCT(CASE WHEN t1.sv_promotion_id IS NOT NULL THEN t1.order_id ELSE NULL END)) AS orders_with_sv,
            COUNT(DISTINCT(CASE WHEN t1.sv_promotion_id IS NOT NULL THEN t1.seller_id ELSE NULL END)) AS sellers_with_sv,
            CAST((SUM(t1.gmv)) AS DECIMAL(20,2)) AS gmv,
            COUNT(DISTINCT CASE WHEN t4.user_status IN ('banned','frozen') THEN t1.seller_id ELSE NULL END) AS qty_sellers_banned_frozen,
			COUNT(DISTINCT CASE WHEN t1.shipping_channel_id IN (90001, 98001, 98002, 90003) THEN t1.shipping_traceno END) AS correios_net_orders,
			COUNT(DISTINCT CASE WHEN t1.shipping_channel_id IN (90001, 98001, 98002, 90003) THEN t6.etiqueta END) AS available_asf,	
			ABS(SUM(
				CASE
					WHEN t5.asf > t1.estimate_shipping_fee THEN t5.asf - t1.estimate_shipping_fee
					ELSE 0
				END)) AS ASF_ESF_dif,
			CASE
                WHEN CAST(t3.is_from_sample_test_wave_1 AS BIGINT) = 1 THEN 'Wave 1'
                WHEN CAST(t3.is_from_sample_test_wave_2 AS BIGINT) = 1 THEN 'Wave 2'
                ELSE 'Allowlisted'
            END AS group_sv
    FROM
        order_mart_dwd_order_all_event_final_status_df AS t1
            INNER JOIN shopee_br.shopee_br_op_team__seller_voucher_sellers_with_feature AS t2 ON CAST(t2.seller_id AS BIGINT) = t1.seller_id
            LEFT JOIN shopee_br.shopee_br_op_team__users_test_blocklist_seller_voucher AS t3 ON CAST(t3.user_id as BIGINT) = t1.seller_id
            LEFT JOIN fraud_tag AS t4 ON t4.user_id = t1.seller_id
            LEFT JOIN asf_final AS t5 ON CAST(t5.tracking_no AS VARCHAR) = CAST(t1.shipping_traceno AS VARCHAR)
			LEFT JOIN shopee_br_anlys.correios_invoice_data AS t6 ON t6.etiqueta = t1.shipping_traceno
    WHERE
        DATE(CAST(FROM_UNIXTIME(t1.create_timestamp) AT TIME ZONE 'America/Sao_Paulo' AS DATE)) >= CURRENT_DATE - INTERVAL '30' DAY
    GROUP BY
        1, 11
)

SELECT DISTINCT
    date,
        total_orders,
        total_sellers,
        sellers_with_sv,
        orders_with_sv,
        qty_sellers_banned_frozen,
        group_sv,
        gmv,
        ASF_ESF_dif,
        correios_net_orders,
		available_asf
FROM
    orders
GROUP BY
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
ORDER BY
    1 DESC
