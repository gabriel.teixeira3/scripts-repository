	WITH orders AS (
		SELECT DISTINCT
			shop_id,
				seller_id,
				COUNT(DISTINCT(order_id)) AS total_sales, -- total completed sales
				MIN(DATE(CAST(create_datetime AS TIMESTAMP))) AS first_sale_date, -- first sale date
				SUM(gmv) AS nmv --gmv = total value nmv = value that will be received
		FROM
			shopee_br.order_mart_dwd_order_item_all_event_final_status_df
		WHERE
			is_cb_shop = 0 -- BR only
				AND complete_timestamp IS NOT NULL
				AND LOWER(order_be_status) NOT IN ('unpaid','invalid', 'return_procesing', 'cancel_pending', 'cancel_completed', 'cancel_processing', 'return_completed') -- filter for nmv
		GROUP BY 1,2
	)

SELECT
	shop_id,
		seller_id
	FROM
		orders
	WHERE
		nmv >= 300
			AND first_sale_date <= DATE((CURRENT_TIMESTAMP AT TIME ZONE 'America/Sao_Paulo' - INTERVAL '15' DAY))
			AND total_sales >= 5
