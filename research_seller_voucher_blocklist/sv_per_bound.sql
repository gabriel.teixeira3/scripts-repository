WITH fraud_tag AS (
	SELECT DISTINCT
	t1.user_id,
		DATE(CAST(registration_datetime AS TIMESTAMP)) AS registration_date,
		MAX_BY(t2.name, t3.ctime) AS fraud_type,
		CASE 
			WHEN t1.status = 0 THEN 'deleted'
			WHEN t1.status = 1 THEN 'normal'
			WHEN t1.status = 2 THEN 'banned'
			WHEN t1.status = 3 THEN 'frozen'
			ELSE NULL 
		END AS user_status
	FROM
		shopee_br.user_mart_dim_user AS t1
			LEFT JOIN shopee_br.shopee_fraud_backend_br_db__fraud_user_tag_tab AS t3 ON (t3.userid = t1.user_id)
			LEFT JOIN shopee_br.shopee_fraud_backend_br_db__fraud_tag_tab AS t2 ON (t2.id = t3.tagid)
	WHERE
		(DATE(t1.grass_date) = DATE(CURRENT_TIMESTAMP AT TIME ZONE 'America/Sao_Paulo' - INTERVAL '48' HOUR))
			AND t3.status = 1
	GROUP BY
		1,2,4
),

orders AS (
	SELECT DISTINCT
		order_id,
			seller_id,
			buyer_id,
			sv_promotion_id,
			DATE(CAST(FROM_UNIXTIME(create_timestamp) AT TIME ZONE 'America/Sao_Paulo' AS DATE)) AS date,
			CAST(CASE WHEN (gmv-sv_rebate_by_seller_amt) = 0 THEN 1 ELSE (1-((gmv-sv_rebate_by_seller_amt)/gmv)) END AS DECIMAL(10,2)) AS bound
	FROM
		order_mart_dwd_order_all_event_final_status_df
	WHERE
		gmv <> 0
			AND sv_promotion_id IS NOT NULL
			AND DATE(CAST(FROM_UNIXTIME(create_timestamp) AT TIME ZONE 'America/Sao_Paulo' AS DATE)) >= CURRENT_DATE - INTERVAL '45' DAY -- Mudar as datas aqui
	GROUP BY
		1,2,3,4,5,6
)

SELECT
	CASE
		WHEN bound >= 0.9 THEN '90-100'
		WHEN bound >= 0.8 and bound < 0.9 THEN '80-90'
		WHEN bound >= 0.7 and bound < 0.8 THEN '70-80'
		WHEN bound >= 0.6 and bound < 0.7 THEN '60-70'
		WHEN bound >= 0.5 and bound < 0.6 THEN '50-60'
		WHEN bound >= 0.4 and bound < 0.5 THEN '40-50'
		WHEN bound >= 0.3 and bound < 0.4 THEN '30-40'
		WHEN bound >= 0.2 and bound < 0.3 THEN '20-30'
		WHEN bound >= 0.1 and bound < 0.2 THEN '10-20'
		WHEN bound < 0.1 THEN '<10'
	END AS discount,
		COUNT(DISTINCT(CASE WHEN sv_promotion_id IS NOT NULL THEN order_id ELSE NULL END)) AS Orders_With_SV,
		COUNT(DISTINCT seller_id) AS Total_Sellers,
		COUNT(DISTINCT buyer_id) AS Total_Buyer,
		COUNT(DISTINCT (CASE WHEN f.user_status in ('banned','frozen') AND f.fraud_type IN ('Free Shipping','Shipping Fee Fraud') THEN o.seller_id END)) AS sellers_banned,
		COUNT(DISTINCT (CASE WHEN f2.user_status in ('banned','frozen') AND f2.fraud_type IN ('Free Shipping','Shipping Fee Fraud') THEN o.buyer_id END)) AS buyers_banned
FROM
	orders AS t1
		LEFT JOIN fraud_tag AS t2 on t2.user_id = t1.seller_id
		LEFT JOIN fraud_tag AS t3 on t3.user_id = t1.buyer_id
GROUP BY
	1
ORDER BY
	1 DESC