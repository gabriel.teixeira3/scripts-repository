SELECT DISTINCT
    DATE(CAST(FROM_UNIXTIME(t1.create_timestamp) AT TIME ZONE 'America/Sao_Paulo' AS DATE)) AS date,
        COUNT(DISTINCT t1.seller_id) AS total_sellers,
        COUNT(DISTINCT(CASE WHEN t1.sv_promotion_id IS NOT NULL THEN t1.seller_id ELSE NULL END)) AS sellers_with_sv,
        COUNT(DISTINCT t1.order_id) AS total_orders,
        COUNT(DISTINCT(CASE WHEN t1.sv_promotion_id IS NOT NULL THEN t1.order_id ELSE NULL END)) AS orders_with_sv,
        CAST((SUM(t1.gmv)) AS DECIMAL(20,2)) AS total_gmv,
        CAST(SUM(CASE WHEN t1.sv_promotion_id IS NOT NULL THEN t1.gmv ELSE 0 END) AS DECIMAL(20,2)) AS gmv_with_sv
FROM
    shopee_br.order_mart_dwd_order_all_event_final_status_df AS t1
		INNER JOIN shopee_br.shopee_br_op_team__seller_voucher_sellers_with_feature AS t2 ON CAST(t2.seller_id AS BIGINT) = t1.seller_id
WHERE
    (DATE(CAST(FROM_UNIXTIME(t1.create_timestamp) AT TIME ZONE 'America/Sao_Paulo' AS DATE)) >= CURRENT_DATE - INTERVAL '30' DAY)
GROUP BY
    1
ORDER BY 1 DESC