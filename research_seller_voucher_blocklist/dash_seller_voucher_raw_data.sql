WITH fraud_tag AS (
    SELECT DISTINCT
        t1.user_id,
            DATE(CAST(registration_datetime AS TIMESTAMP)) AS registration_date,
            MAX_BY(t2.name, t3.ctime) AS fraud_type,
            CASE   
                WHEN t1.status = 0 THEN 'deleted'
                WHEN t1.status = 1 THEN 'normal'
                WHEN t1.status = 2 THEN 'banned'
                WHEN t1.status = 3 THEN 'frozen'
                ELSE NULL 
            END AS user_status
    FROM 
        shopee_br.user_mart_dim_user AS t1
            LEFT JOIN shopee_br.shopee_fraud_backend_br_db__fraud_user_tag_tab AS t3 ON (t3.userid = t1.user_id)
            LEFT JOIN shopee_br.shopee_fraud_backend_br_db__fraud_tag_tab AS t2 ON (t2.id = t3.tagid)
    WHERE 
        (DATE(t1.grass_date) = DATE(CURRENT_TIMESTAMP AT TIME ZONE 'America/Sao_Paulo' - INTERVAL '48' HOUR)) 
            AND t3.status = 1
    GROUP BY 
        1,2,4
),

orders AS (
    SELECT DISTINCT
        t1.seller_id,
            COUNT(DISTINCT t1.order_id) AS Total_Orders,
            COUNT(DISTINCT(CASE WHEN t1.sv_promotion_id IS NOT NULL THEN t1.order_id ELSE NULL END)) AS Orders_With_SV,
            CAST((SUM(t1.gmv)) AS DECIMAL(20,2)) AS gmv,
            COUNT(DISTINCT CASE WHEN t2.user_status IN ('banned','frozen') THEN t1.seller_id ELSE NULL END) AS qty_sellers_banned_frozen
    FROM
        shopee_br.order_mart_dwd_order_all_event_final_status_df AS t1
        LEFT JOIN fraud_tag AS t2 ON t2.user_id = t1.seller_id
    WHERE
        (DATE(CAST(FROM_UNIXTIME(t1.create_timestamp) AT TIME ZONE 'America/Sao_Paulo' AS DATE)) >= CURRENT_DATE - INTERVAL '30' DAY)
    GROUP BY
    1
),

final_tb AS (
    SELECT DISTINCT
        t0.seller_id,
            CASE 
                WHEN t3.Orders_With_SV > 0 THEN 1
                ELSE 0
            END AS used_sv,
            IF(t3.Total_Orders IS NULL,0,t3.Total_Orders) AS total_orders,
            IF(t3.Orders_With_SV IS NULL,0,t3.Orders_With_SV) AS orders_with_sv,
            IF(t3.gmv IS NULL,0,t3.gmv) AS gmv,
            CASE 
                WHEN t2.user_status IN ('banned') THEN 1 
                ELSE 0 
            END AS is_banned,
            CASE 
                WHEN t2.user_status IN ('frozen') THEN 1 
                ELSE 0 
            END AS is_frozen,
            CASE 
                WHEN t2.user_status IN ('banned','frozen') THEN t2.fraud_type 
                ELSE NULL 
            END AS fraud_type,
            CASE 
                WHEN CAST(t1.is_from_sample_test_wave_1 AS BIGINT) = 1 THEN 'Wave 1'
                WHEN CAST(t1.is_from_sample_test_wave_2 AS BIGINT) = 1 THEN 'Wave 2'
                ELSE 'Allowlisted'
            END AS group_sv
    FROM 
        shopee_br.shopee_br_op_team__seller_voucher_sellers_with_feature AS t0
            LEFT JOIN shopee_br.shopee_br_op_team__users_test_blocklist_seller_voucher AS t1 ON CAST(t1.user_id AS BIGINT) = CAST(t0.seller_id AS BIGINT)
            LEFT JOIN fraud_tag AS t2 ON t2.user_id = CAST(t0.seller_id AS BIGINT)
            LEFT JOIN orders AS t3 ON t3.seller_id = CAST(t0.seller_id AS BIGINT)
)

SELECT
    seller_id,
        used_sv,
        total_orders,
        orders_with_sv,
        gmv,
        is_banned,
        is_frozen,
        fraud_type,
        group_sv
FROM
    final_tb
ORDER BY 
    4 DESC
