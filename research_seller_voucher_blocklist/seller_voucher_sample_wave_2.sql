WITH saldo AS (
	SELECT DISTINCT
		t1.userid,
			(t1.available/100000.00) AS available
	FROM
		shopee_br.shopee_br_db__wallet_tab AS t1
			LEFT JOIN user_mart_dim_user AS t2 ON t2.user_id = t1.userid
	WHERE
		DATE(t2.grass_date) = CURRENT_DATE - INTERVAL '2' DAY
			AND available < 0
),

dates AS (
	SELECT
		date_trunc('month', DATE(current_timestamp AT TIME ZONE 'America/Sao_Paulo')) AS init_date
),

users AS (
	SELECT
		grass_date,
			user_id,
			shop_id,
			status AS shop_status,
			CASE
				WHEN ABS(CAST(split(CAST((grass_date - date(from_unixtime(last_login_timestamp, 'America/Sao_Paulo'))) AS VARCHAR), ' ')[1] AS BIGINT)) BETWEEN 0 AND 15 THEN 1
				ELSE NULL
			END AS index,
	FROM
		user_mart_dim_user
	WHERE
		grass_date >= (select init_date from dates)
			AND is_seller = 1
			AND status = 1
),

items AS (
	SELECT
		items.grass_date,
			items.shop_id,
			items.item_id,
			items.shop_status,
			items.status AS item_status,
			items.stock AS item_stock,
			items.is_holiday_mode,
			items.is_cb_shop,
			DATE(from_unixtime(items.create_timestamp) AT TIME ZONE 'America/Sao_Paulo') AS item_create_date
	FROM
		shopee_br.item_mart_dim_item items
	WHERE
		grass_date >= (select init_date from dates)
),

seller_type AS (
	SELECT
		CAST(t1.shop_id AS BIGINT) AS shop_id,
			CASE WHEN t2.partner_id IS NOT NULL THEN 'ERP' ELSE t1.tax_id END AS tax_type
	FROM
		shopee_br.shopee_br_bi_team__seller_taxid AS t1
			LEFT JOIN shopee_br.shopee_partner_db__partner_shop_tab AS t2 ON CAST(t1.shop_id AS BIGINT) = t2.shopid
),

active_seller AS (
	SELECT DISTINCT
		users.grass_date,
			items.is_cb_shop,
			users.shop_id
	FROM
		users
			JOIN items ON users.shop_id = items.shop_id AND users.grass_date = items.grass_date
	WHERE
		items.item_create_date <= DATE(current_timestamp AT TIME ZONE 'America/Sao_Paulo') - INTERVAL '1' DAY
			AND (items.is_holiday_mode = 0 OR items.is_holiday_mode IS NULL)
			AND users.index = 1
			AND items.item_status = 1
			AND items.shop_status = 1
			AND items.item_stock > 0

	UNION

	SELECT DISTINCT
		items.grass_date,
			items.is_cb_shop,
			items.shop_id
	FROM
		items
	WHERE
		item_create_date = DATE(current_timestamp AT TIME ZONE 'America/Sao_Paulo') - INTERVAL '1' DAY
),

sellers_product AS (
	SELECT
		seller_id,
			COUNT(DISTINCT item_id) AS quantity_registered_product
	FROM
		shopee_br.item_mart_dim_item
	WHERE
		grass_date >= DATE((CURRENT_TIMESTAMP AT TIME ZONE 'America/Sao_Paulo' - INTERVAL '1' DAY))
	GROUP BY 
		1
),

bad_sellers_tb AS (
	SELECT DISTINCT
		t0.user_id
	FROM
		shopee_br.user_mart_dim_user AS t0
			LEFT JOIN shopee_cap_br_db__cap_session_voucher_tab AS t1 ON t1.shop_id = t0.shop_id
			LEFT JOIN saldo AS t2 ON t2.userid = t0.user_id
			LEFT JOIN active_seller AS t3 ON t3.shop_id = t0.shop_id
			LEFT JOIN sellers_product AS t4 ON t4.seller_id = t0.user_id
	WHERE
		(DATE_DIFF('day', DATE(SPLIT(t0.registration_datetime, ' ')[1]), CURRENT_DATE) <= 15 -- Conta Nova
			OR t1.shop_penalty >= 3 -- Penalty Points
			OR t2.userid IS NOT NULL -- Saldo Negativo
			OR t3.shop_id IS NULL -- Seller Inativo
			OR t4.quantity_registered_product < 5) -- Qtd Produtos
			AND t0.is_seller = 1
			AND t0.grass_date = CURRENT_DATE - INTERVAL '2' DAY
),

seller_on_allowlist AS (
	WITH orders AS (
		SELECT DISTINCT
			shop_id,
				seller_id,
				COUNT(DISTINCT(order_id)) AS total_sales, -- total completed sales
				MIN(DATE(CAST(create_datetime AS TIMESTAMP))) AS first_sale_date, -- first sale date
				SUM(gmv) AS nmv --gmv = total value nmv = value that will be received
		FROM
			shopee_br.order_mart_dwd_order_item_all_event_final_status_df
		WHERE
			is_cb_shop = 0 -- BR only
				AND complete_timestamp IS NOT NULL
				AND LOWER(order_be_status) NOT IN ('unpaid','invalid', 'return_procesing', 'cancel_pending', 'cancel_completed', 'cancel_processing', 'return_completed') -- filter for nmv
		GROUP BY 1,2
	)

SELECT
	shop_id,
		seller_id
	FROM
		orders
	WHERE
		nmv >= 300
			AND first_sale_date <= DATE((CURRENT_TIMESTAMP AT TIME ZONE 'America/Sao_Paulo' - INTERVAL '15' DAY))
			AND total_sales >= 5

)

SELECT DISTINCT
	t0.user_id
FROM 
	shopee_br.user_mart_dim_user AS t0
		LEFT JOIN bad_sellers_tb t1 ON t1.user_id = t0.user_id
		LEFT JOIN seller_on_allowlist t2 ON t2.seller_id = t0.user_id
		LEFT JOIN shopee_br_op_team__users_test_blocklist_seller_voucher t3 ON CAST(t3.user_id AS BIGINT)= t0.user_id
WHERE 
	is_seller = 1
		AND grass_date = CURRENT_DATE - INTERVAL '2' DAY
		AND t1.user_id IS NULL
		AND t2.seller_id IS NULL
		AND CAST(t3.user_id AS BIGINT) IS NULL
		AND is_cb_shop = 0
