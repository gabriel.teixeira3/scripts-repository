--amount in BRL
WITH 
xrate as (
    SELECT
        grass_date date
        , exchange_rate
    FROM shopee_br.order_mart_dim_exchange_rate
    WHERE ((grass_region = 'BR') AND (grass_date BETWEEN "date_add"('day', -1000, CAST(current_timestamp AT TIME ZONE 'America/Sao_Paulo' AS date)) AND "date_add"('day', -2, CAST(current_timestamp AT TIME ZONE 'America/Sao_Paulo' AS date))))
    GROUP BY 1, 2
    )
SELECT DISTINCT 
    DATE_TRUNC('MONTH', DATE(CAST(t3.create_datetime AS TIMESTAMP))) transaction_month
    , DATE_TRUNC('WEEK', DATE(CAST(t3.create_datetime AS TIMESTAMP))) transaction_week
    , DATE(CAST(t3.create_datetime AS TIMESTAMP)) transaction_date
    , DATE_TRUNC('MONTH', DATE(CAST(chargeback_date AS TIMESTAMP))) cbk_month
    , DATE_TRUNC('WEEK', DATE(CAST(chargeback_date AS TIMESTAMP))) cbk_week
    , DATE(chargeback_date) cbk_date
    , COUNT(DISTINCT CASE WHEN payment_method_id = 38 then checkout_id ELSE NULL END) total_boleto_checkout
    , COUNT(DISTINCT CASE WHEN payment_method_id = 1 then checkout_id ELSE NULL END) total_cc_checkout
    , SUM(CASE WHEN payment_method_id = 38 then CAST(gmv AS DECIMAL(20,2)) ELSE 0 END) boleto_gmv
    , SUM(CASE WHEN payment_method_id = 38 and order_be_status_id in (2,4,11,12,13,14,15) then CAST(gmv AS DECIMAL(20,2)) ELSE 0 END) boleto_nmv
    , SUM(CASE WHEN payment_method_id = 1 then CAST(gmv AS DECIMAL(20,2)) ELSE 0 END) cc_gmv
    , SUM(CASE WHEN payment_method_id = 1 and order_be_status_id in (2,4,11,12,13,14,15) then CAST(gmv AS DECIMAL(20,2)) ELSE 0 END) cc_nmv
    , COUNT(DISTINCT pmt_hash) total_cbk
    , SUM(CAST(amount_chargeback AS DECIMAL(20,2))) total_amount_cbk
    , COUNT(DISTINCT (CASE WHEN description IN ('Fraud (card-not-present)/No cardholder authorisation'
        ,'Instalment dispute / Fraudulent Multiple Transactions'
        , 'Fraud (card-not-present)/No cardholder authorization'
        , 'Transaction not recognised'
        ) THEN pmt_hash ELSE NULL END)) fraud_cbk
    , SUM(CASE WHEN description IN ('Fraud (card-not-present)/No cardholder authorisation'
        ,'Instalment dispute / Fraudulent Multiple Transactions'
        , 'Fraud (card-not-present)/No cardholder authorization'
        , 'Transaction not recognised'
        ) THEN CAST(amount_chargeback AS DECIMAL(20,2)) ELSE 0 END) fraud_amount_cbk
    , SUM(CASE WHEN pmt_hash IS NOT NULL AND is_cb_shop = 1 then cast(5*exchange_rate AS DECIMAL (20,2))
        WHEN pmt_hash IS NOT NULL AND is_cb_shop = 0 then cast(20 AS DECIMAL(20,2)) END) AS cbk_fee
    , SUM(CASE WHEN description IN ('Fraud (card-not-present)/No cardholder authorisation'
        ,'Instalment dispute / Fraudulent Multiple Transactions'
        , 'Fraud (card-not-present)/No cardholder authorization'
        , 'Transaction not recognised'
        ) AND pmt_hash IS NOT NULL AND is_cb_shop = 1 then cast(5*exchange_rate AS DECIMAL (20,2))
        WHEN description IN ('Fraud (card-not-present)/No cardholder authorisation'
        ,'Instalment dispute / Fraudulent Multiple Transactions'
        , 'Fraud (card-not-present)/No cardholder authorization'
        , 'Transaction not recognised'
        ) AND pmt_hash IS NOT NULL AND is_cb_shop = 0 then cast(20 AS DECIMAL(20,2)) END) AS cbk_fee_fraud
    , COUNT(DISTINCT CASE WHEN description IN ('Fraud (card-not-present)/No cardholder authorisation'
        ,'Instalment dispute / Fraudulent Multiple Transactions'
        , 'Fraud (card-not-present)/No cardholder authorization'
        , 'Transaction not recognised'
        ) AND pmt_hash IS NOT NULL AND dispute_s_status = 'Dispute Won' then pmt_hash END) total_cbk_dispute_won
    , SUM(CASE WHEN description IN ('Fraud (card-not-present)/No cardholder authorisation'
        ,'Instalment dispute / Fraudulent Multiple Transactions'
        , 'Fraud (card-not-present)/No cardholder authorization'
        , 'Transaction not recognised'
        ) AND pmt_hash IS NOT NULL AND dispute_s_status = 'Dispute Won' then CAST(amount_chargeback AS DECIMAL(20,2)) END) amount_dispute_won
FROM shopee_br.order_mart_dwd_order_all_event_final_status_df as t3
    LEFT JOIN shopee_br.shopee_payment_module_payment_br_db__payment_v2_tab as t2 ON t3.payment_transaction_id = t2.transaction_id 
    LEFT JOIN shopee_br_anlys.ebanx_chargebacks t1 on t1.pmt_hash = t2.channel_ref
    LEFT JOIN xrate t4 ON date(t1.chargeback_date) = t4.date
WHERE t2.channel_ref is not null
    AND t2.payment_type = 4
    AND DATE(CAST(t3.create_datetime AS TIMESTAMP)) >= DATE(current_timestamp at time zone 'America/Sao_Paulo' - interval '180' day)
GROUP BY 1, 2, 3, 4, 5, 6
ORDER BY 1 DESC, 2 DESC, 3 DESC
