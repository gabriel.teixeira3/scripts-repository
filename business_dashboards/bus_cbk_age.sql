WITH cbk_base AS (
    SELECT DISTINCT 
        pmt_hash
        , DATE(CAST(t3.create_datetime AS TIMESTAMP)) transaction_date
        , DATE(chargeback_date) cbk_date
        , date_diff('day', CAST(t3.create_datetime AS TIMESTAMP),CAST(chargeback_date AS TIMESTAMP)) cbk_age
        , CASE WHEN date_diff('day', CAST(t3.create_datetime AS TIMESTAMP),CAST(chargeback_date AS TIMESTAMP)) <7 THEN 'a.<7d'
            WHEN date_diff('day', CAST(t3.create_datetime AS TIMESTAMP),CAST(chargeback_date AS TIMESTAMP)) BETWEEN 7 AND 14 THEN 'b.>=7d and <15d'
            WHEN date_diff('day', CAST(t3.create_datetime AS TIMESTAMP),CAST(chargeback_date AS TIMESTAMP)) BETWEEN 15 AND 29 THEN 'c.>=15d and <30d'
            WHEN date_diff('day', CAST(t3.create_datetime AS TIMESTAMP),CAST(chargeback_date AS TIMESTAMP)) BETWEEN 30 AND 59 THEN 'd.>=30d and <60d'
            WHEN date_diff('day', CAST(t3.create_datetime AS TIMESTAMP),CAST(chargeback_date AS TIMESTAMP)) BETWEEN 60 AND 89 THEN 'e.>=60 and <90'
            WHEN date_diff('day', CAST(t3.create_datetime AS TIMESTAMP),CAST(chargeback_date AS TIMESTAMP)) BETWEEN 90 AND 119 THEN 'f.>=90 and <120'
            WHEN date_diff('day', CAST(t3.create_datetime AS TIMESTAMP),CAST(chargeback_date AS TIMESTAMP)) BETWEEN 120 AND 149 THEN 'g.>=120 and <150'
            WHEN date_diff('day', CAST(t3.create_datetime AS TIMESTAMP),CAST(chargeback_date AS TIMESTAMP)) BETWEEN 149 AND 179 THEN 'h.>=150 and <180'
            WHEN date_diff('day', CAST(t3.create_datetime AS TIMESTAMP),CAST(chargeback_date AS TIMESTAMP)) > 180 THEN 'i.>180d'
        ELSE NULL END AS cbk_age_range
        , CAST(amount_chargeback AS DECIMAL(20,2)) amount_chargeback
        , CASE WHEN description IN ('Fraud (card-not-present)/No cardholder authorisation'
            ,'Instalment dispute / Fraudulent Multiple Transactions'
            , 'Fraud (card-not-present)/No cardholder authorization'
            , 'Transaction not recognised'
            ) THEN 1 ELSE 0 END is_fraud_cbk
    FROM shopee_br_anlys.ebanx_chargebacks t1
        LEFT JOIN shopee_br.shopee_payment_module_payment_br_db__payment_v2_tab as t2 on t1.pmt_hash = t2.channel_ref
        LEFT JOIN  shopee_br.order_mart_dwd_order_all_event_final_status_df as t3 ON t3.payment_transaction_id = t2.transaction_id 
    WHERE t2.channel_ref is not null
        AND t2.payment_type = 4
        AND DATE(CAST(t3.create_datetime AS TIMESTAMP)) >= DATE(current_timestamp at time zone 'America/Sao_Paulo' - interval '180' day)
        AND CAST(amount_chargeback AS DECIMAL(20,2)) > 0.00 --refunded cbk
    )
SELECT DISTINCT 
    transaction_date
    , cbk_date
    , cbk_age_range
    , COUNT(DISTINCT pmt_hash) total_cbk
    , SUM(amount_chargeback) total_cbk_amount
FROM cbk_base
GROUP BY 1, 2, 3
ORDER BY 1 DESC
    
    